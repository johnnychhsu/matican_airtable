from airtable import airtable
import requests
import json

BASE_ID = 'appxcquoaRlqUOzS0'
API_KEY = 'keyo1mWLEskHx9KTg'
TABLE_NAME = 'Reference ASIN'

at = airtable.Airtable(BASE_ID, API_KEY)
a = at.get(TABLE_NAME)

for row in a['records']:

    _data = dict(row['fields'])
    _record_id = row['id']
    if 'API Done' in _data and _data['API Done'] == 'True':
        continue
    else:
        _asin =  _data['ASIN']
        if len(_asin) == 10:
            r = requests.get('https://amzscout.net/api/v1/landing/fees?asin={}&domain=COM'.format(_asin))
            r_dict = json.loads(r.text) if r.text else None
            _product = r_dict['product'] if 'product' in r_dict else None
            _fee = r_dict['fees'] if 'fees' in r_dict else None


            _update_dict = {
                "Title": _product['name'],
                "Img": [{"url": _product['image']}],
                "Landed Price": _product['price'],
                "FBA Fee": _fee['pickAndPack'],
                "Monthly Storage Fees": _fee['storage'],
                "width": _product['size']['width'],
                "height": _product['size']['height'],
                "depth": _product['size']['depth'],
                "shippingWeight": _product['shippingWeight'],
                "API Done": "True"
            }

            at.update(TABLE_NAME, _record_id, _update_dict)
