from airtable import airtable
from mysql_helper import MySQLHelper
import requests
import json

BASE_ID = 'appZLURfSplmSqjJs'
API_KEY = 'keyo1mWLEskHx9KTg'
TABLE_NAME = 'Product Meta Data'

def main():
    at = airtable.Airtable(BASE_ID, API_KEY)
    a = at.get(TABLE_NAME)
    sku_data = get_sku_data()

    for row in a['records']:
        _data = dict(row['fields'])
        _record_id = row['id']
        _sku = _data['SKU']
        if _sku in sku_data:
            _update_dict = sku_data[_sku]
            print(_update_dict)
            at.update(TABLE_NAME, _record_id, _update_dict)


def get_sku_data():
    mysql = MySQLHelper(
        schema="mws_report_api",
        db_conf={
            "host": "34.233.90.155",
            "port": 3306,
            "user": "johnny",
            "passwd": "makemoney16888"
        }
    )

    query = '''
    SELECT t0.*, ifnull(t1.quantity, 0) AS quantity_sold
    FROM (
        SELECT 
            sku
            , longest_side
            , median_side
            , shortest_side
            , item_package_weight
            , product_size_tier
            , est_fee_total - est_referral_fee_per_unit - est_variable_closing_fee AS est_fba_fee

        FROM mws_fee_preview
        WHERE 1=1
            AND (sku, data_set) IN (
                SELECT sku, max(data_set)
                FROM mws_fee_preview
                WHERE 1=1
                GROUP BY sku
            )
    ) AS t0
    LEFT JOIN (
        SELECT sku,sum(quantity) AS quantity
        FROM sales_order
        WHERE 1=1
            AND purchase_date >= "2018-08-01"
        GROUP BY sku
    ) AS t1
    ON t0.sku = t1.sku
    '''
    result = mysql.execute(query=query, cursor=mysql.CursorType.DICT_CURSOR)
    output = {}
    for row in result:
        if row['sku'] not in output:
            output[row['sku']] = {}

        # output[row['sku']]['Package Longest Side'] = float(row['longest_side'])
        # output[row['sku']]['Package Median Side'] = float(row['median_side'])
        # output[row['sku']]['Package Shortest Side'] = float(row['shortest_side'])
        # output[row['sku']]['Package Weight'] = float(row['item_package_weight'])
        # output[row['sku']]['Size Tier'] = row['product_size_tier']
        # output[row['sku']]['FBA Fee'] = float(row['est_fba_fee'])
        output[row['sku']]['Historical Sold Qty'] = int(row['quantity_sold'])

    return output

if __name__ == '__main__':
    main()
