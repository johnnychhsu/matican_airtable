#!/usr/bin/env python3
import pymysql
import traceback
import time
from multiprocessing import Lock
mysql_lock = Lock()


def db_retry(retry_error_code=None, retry_times=3, sleep_time=1):
    def wrap(func):
        def wrapped_func(*args, **kwargs):
            retry = retry_times
            while True:
                try:
                    return_error_code = None
                    result = func(*args, **kwargs)
                except pymysql.Error as e:
                    return_error_code = e.args[0]
                    error_description = e.args[1]
                    error = e
                finally:
                    if return_error_code:
                        if return_error_code != retry_error_code or retry == 0:
                            raise error
                        else:
                            print("Remain Retries: %s" % retry)
                            retry -= 1
                            time.sleep(sleep_time)
                    else:
                        return result
        return wrapped_func
    return wrap


class MySQLErrorCode:
    LOCK_WAIT_TIMEOUT = 1205
    INNODB_DEADLOCK = 1213


class MySQLHelper:
    _connections = {}

    class CursorType:
        NORMAL_CURSOR = pymysql.cursors.Cursor
        DICT_CURSOR = pymysql.cursors.DictCursor

    def __init__(self, schema=None, db_conf=dict()):
        if not schema in MySQLHelper._connections:
            if schema is not None:
                db_conf['db'] = schema
            self._conn = pymysql.connect(host=db_conf['host'],
                                         port=db_conf['port'],
                                         user=db_conf['user'],
                                         passwd=db_conf['passwd'],
                                         db=db_conf['db'],
                                         charset='utf8')
            self._cursor = self._conn.cursor(self.CursorType.NORMAL_CURSOR)
            MySQLHelper._connections[schema] = {}
            MySQLHelper._connections[schema]['conn'] = self._conn
            MySQLHelper._connections[schema]['_cursor'] = self._cursor
            print('Establish MySQL connection ({}:{}) for schema ({}) done.'.format(db_conf['host'], db_conf['port'], schema))
        else:
            self._conn = MySQLHelper._connections[schema]['conn']
            self._cursor = MySQLHelper._connections[schema]['_cursor']

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def set_cursor(self, cursor):
        self._cursor = self._conn.cursor(cursor)

    def cursor(self):
        return self._cursor

    @db_retry(MySQLErrorCode.INNODB_DEADLOCK, 3, 1)
    def executemany(self, query, data):
        mysql_lock.acquire()
        print('Acquire lock in executemany()...')
        self._cursor.executemany(query, data)
        self._conn.commit()
        mysql_lock.release()
        print('Release lock in executemany()...')
        return self._cursor

    @db_retry(MySQLErrorCode.INNODB_DEADLOCK, 3, 1)
    def execute(self, query, cursor=CursorType.NORMAL_CURSOR, data=None):
        mysql_lock.acquire()
        print('Acquire lock in execute()...')
        self._cursor = self._conn.cursor(cursor)
        if data is not None:
            query = query.format(**data)
        try:
            self._cursor.execute(query)
            self._conn.commit()
        except:
            raise
        finally:
            mysql_lock.release()
            print('Release lock in execute()...')
        return self._cursor

    def close(self):
        self._cursor.close()
        self._conn.close()

    def get_table_name(self, tablelike):
        query = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA = '{schema}' AND TABLE_NAME LIKE '{tablelike}' ORDER BY TABLE_NAME DESC".format(**{
            "schema": 'juvoimd',
            "tablelike": tablelike
        })
        result = self.execute(query)
        if result.rowcount == 0:
            return None
        else:
            return [row[0] for row in result.fetchall()]